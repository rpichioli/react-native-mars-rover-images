import React from 'react';
import { Platform } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import devTools from 'remote-redux-devtools';
import promise from 'redux-promise';
import thunk from 'redux-thunk';
import logger from 'redux-logger';

import rootReducer from './src/reducers/index';

import Header from './src/components/Header';
import Main from './src/components/Main';
import PhotoDetails from './src/components/PhotoDetails';
// import Details from './src/components/Details';

const store = createStore(
	rootReducer,
	compose(
		applyMiddleware(promise, thunk),
		devTools({ name: Platform.OS, hostname: 'localhost', port: 9001 })
	)
);

const RootStack = createStackNavigator(
	{
	  Home: Main, // Renders photos from Mars Rover in a gallery style
	  PhotoDetails: PhotoDetails, // Entering a photo you can see details about it, official content by Nasa
	}, {
    initialRouteName: 'Home',
  }
);

const Navigation = createAppContainer(RootStack);

export default class App extends React.Component {
	render() {
	  return (
			<Provider store={store}>
				<Header />
				<Navigation />
			</Provider>
	  );
	}
}
