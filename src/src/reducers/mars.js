import { FETCH_REQUEST, FETCH_SUCCESS, FETCH_FAILED } from '../utils/types.js';

const initialState = {
	isFetching: false,
	data: [],
	hasError: false,
	errorMessage: null
}

export default function(state = initialState, action = {}) {
	switch(action.type) {
		case FETCH_REQUEST:
			return {
				isFetching: true,
				data: [],
				hasError: false,
				errorMessage: null
			};
		case FETCH_SUCCESS:
			return {
				isFetching: false,
				data: action.payload,
				hasError: false,
				errorMessage: null
			};
		case FETCH_FAILED:
			return {
				isFetching: false,
				data: [],
				hasError: true,
				errorMessage: action.err
			};
		default:
			return state;
	}
}
