import { combineReducers } from 'redux';
import mars from './mars';

export default combineReducers({
	mars
});
