export const APIBaseURI = 'https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos';
export const APIKey = 'YOUR_API_KEY';