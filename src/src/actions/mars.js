import { APIBaseURI, APIKey } from './../utils/constants';
import { FETCH_REQUEST, FETCH_SUCCESS, FETCH_FAILED } from './../utils/types.js';

export function fetchAll() {
	return async dispatch => {
		dispatch({ type: FETCH_REQUEST });
		try {
			let response = await fetch(`${APIBaseURI}?sol=1000&api_key=${APIKey}`);
			if (response.ok) {
				let data = await response.json();
				dispatch({ type: FETCH_SUCCESS, payload: data.photos });
			} else {
				dispatch({ type: FETCH_FAILED, error: "Can't fetch any data." });
			}
		} catch (e) {
			dispatch({ type: FETCH_FAILED, error: e.message });
		}
	}
}
