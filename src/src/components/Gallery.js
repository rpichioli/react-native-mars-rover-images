import React from 'react';
import { ScrollView, View, Text, Image, StyleSheet, TouchableHighlight } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';

class Gallery extends React.Component {
	render() {
		let { mars, navigation } = this.props;
		let data = mars.data;
		data = data.slice(0,9); // Selecting certain number of lines

		if (data.length === 0) {
			return (
				<View>
					<Spinner
						visible={mars.isFetching}
						textContainer={"Loading..."}
						textStyle={{color: '#ffffff'}}
						animation='fade'
					/>
				</View>
			);
		}

		return (
			<ScrollView contentContainerStyle={styles.container}>
				{ data.map((item, index) => {
					return (
						<TouchableHighlight key={index} onPress={() => navigation.navigate('PhotoDetails', { id: item.id })}>
							<Image
								source={{uri: item.img_src}}
								style={styles.item} />
						</TouchableHighlight>
					)
				}) }
			</ScrollView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
	},
	item: {
		height: 200,
		resizeMode : 'cover',
		padding: 3,
		borderWidth: 1,
		borderColor: '#000000',
		backgroundColor: '#555'
	}
})

export default Gallery;
