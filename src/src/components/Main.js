import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { connect } from 'react-redux';
import { fetchAll } from '../actions/mars';

import Gallery from './Gallery';

class Main extends React.Component {
	static navigationOptions = {
    header: null // Hide navigation bar
  }

	componentDidMount() {
		this.props.fetchAll();
	}

	render() {
		const { mars, navigation } = this.props;
		return (
			<View contentContainerStyle={styles.container}>
				<Gallery mars={mars} navigation={navigation} />
			</View>
		)
	}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#333333',
    alignItems: 'center',
    justifyContent: 'center',
  }
});

const mapStateToProps = (state, props) => {
	return {
		mars: state.mars || {}
	}
}

export default connect(mapStateToProps, { fetchAll })(Main);
