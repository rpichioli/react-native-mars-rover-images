import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';

const Header = () => {
	return (
		<View style={styles.container}>
			<Text style={styles.header}>Mars Rover photos</Text>
		</View>
	)
}

const styles = StyleSheet.create({
	container: {
		display: 'flex',
		paddingTop: 20,
		paddingBottom: 20,
		alignItems: 'center',
		backgroundColor: '#333333'
	},
	header: {
		color: '#eeeeee',
		fontWeight: 'bold',
		fontSize: 24,
		paddingTop: 20,
		textTransform: 'uppercase'
	}
});

export default Header;
