import React from 'react';
import { connect } from 'react-redux';
import { ScrollView, View, Text, Image, Button, StyleSheet } from 'react-native';

class PhotoDetails extends React.Component {
	static navigationOptions = {
		header: null, // Hide navigation bar
		headerLeft: null
	}

	render() {
		let { data, navigation } = this.props;

		return (
			<ScrollView>
				<Image source={{uri: data.img_src}} style={styles.photo} />

				<View style={styles.container}>
					<Text style={styles.title}>Photo's Camera</Text>
					<Text style={styles.text}>{ data.camera.name } - { data.camera.full_name }</Text>
					<Text style={styles.text}>Earth date: { new Date(data.earth_date).toLocaleDateString('pt-br') }</Text>
					<Text style={styles.text}>Sol: { data.sol }</Text>

					<View style={styles.divider}></View>

					<Text style={styles.title}>Rover</Text>
					<Text style={styles.text}>{ data.rover.name }</Text>
					<Text style={styles.text}>Launch date: { new Date(data.rover.launch_date).toLocaleDateString('pt-br') }</Text>
					<Text style={styles.text}>Landing date: { new Date(data.rover.landing_date).toLocaleDateString('pt-br') }</Text>
					<Text style={styles.text}>Status: { data.rover.status }</Text>
					<Text style={styles.text}>Max sol: { data.rover.max_sol }</Text>
					<Text style={styles.text}>Max date: { new Date(data.rover.max_date).toLocaleDateString('pt-br') }</Text>
					<Text style={styles.text}>Total photos: { data.rover.total_photos }</Text>

					<View style={styles.divider}></View>

					<Text style={styles.title}>Rover's Cameras</Text>
					{ data.rover.cameras.map((camera, index) => (<Text key={index} style={styles.text}>{ camera.name } - { camera.full_name }</Text>)) }
				</View>

				<Button
					title="Back to main gallery"
					style={styles.button}
					onPress={() => this.props.navigation.goBack()}
				/>
			</ScrollView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		margin: 10
	},
	divider: {
		marginBottom: 8,
		paddingBottom: 8,
		borderBottomWidth: 1,
		borderBottomColor: '#dddddd'
	},
	title: {
		// marginTop: 10,
		fontSize: 20,
		fontWeight: 'bold'
	},
	text: {
		fontSize: 14
	},
	photo: {
		height: 200,
		resizeMode : 'cover',
		padding: 3,
		borderWidth: 1,
		borderColor: '#000000',
		backgroundColor: '#555'
	},
	button: {
		fontSize: 18,
		padding: 5
	}
});

const mapStateToProps = (state, props) => {
	return {
		data: state.mars.data.filter(item => parseInt(item.id) === parseInt(props.navigation.getParam('id', '-1')))[0] || {}
	}
}

export default connect(mapStateToProps, {})(PhotoDetails);
