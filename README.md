# react-native-mars-rover-images
Awesome app in development!

## Environment
First of all, if you haven't run React Native before I suggest you to take a look in the [official docs](https://facebook.github.io/react-native/docs/getting-started.html) to better build your environment

## Main idea
React-native app using redux state container under the hood.

We are consuming Nasa API fetching for Mars Rover Photos with their whole information.

Built in a scope of 2 views, we can see the photos in first of them that is a gallery in a ScrollView and, by pressing a photo, the app redirects to it's details in another view.

## Structural organization 

React Native files organization under `src` folder - ignoring structural and internal ones:

```bash
├── src
│   ├── actions
│   │   ├── mars.js
│   ├── components
│   │   ├── Gallery.js
│   │   ├── Header.js
│   │   ├── Main.js
│   │   ├── PhotoDetails.js
│   ├── reducers
│   │   ├── index.js
│   │   ├── mars.js
│   └── utils
│       ├── constants.js
│       └── types.js
└── App.js
```

## Files detailment

#### App.js
Application entrypointm the main file. 
We define in this layer the app Navigation, whole Redux (store and middlewares) and set container components in JSX.

#### actions/
- **mars.js** - In this layer we dispatch to Redux a isFetching state before calling API and dispatch results if they are valid, or dispatch error if something wrong happens.

#### reducers/
- **index.js** - Root reducer combining all reducer instances sending to the middleware a single one with all of them.
- **mars.js** - Reducer responsible for Mars Rover Photos storage, handling it's specific dispatched actions.

## Next steps
- Filter inputs (quantity of data to be searched, camera, and so on)
- About view with app, repository and author details 
- Mark favorite images
- Favorite images management view
- Navigation menu

## Author
Rodrigo Quiñones Pichioli, [rpichioli](https://gitlab.com/rpichioli) - 2019
